package database

import (
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func databaseConfig() *gorm.DB {
	db, err := gorm.Open(sqlite.Open("store_book.db"), &gorm.Config{
		QueryFields: true,
	})

	if err != nil {
		panic("Failed to connection")
	}

	return db
}

var DB = databaseConfig()
