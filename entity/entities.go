package entity

import (
	"database/sql"

	"gorm.io/gorm"
)

// StoreBook
//
type User struct {
	gorm.Model
	Email   string         `json:"email" gorm:"index:idx_user_email,unique"`
	Name    string         `json:"name"`
	IsStaff sql.NullBool   `json:"isStaff" gorm:"default:false"`
	Gender  sql.NullString `json:"gender" gorm:"default:''"`
	Books   []*Book        `json:"books" gorm:"foreignKey:AuthorID"`
}

type Book struct {
	gorm.Model
	Title       string  `json:"title" gorm:"index:idx_book_title,unique"`
	Description string  `json:"description"`
	AuthorID    uint    `json:"author_id"`
	Metas       []*Meta `json:"metas" gorm:"foreignKey:BookID"`
}

type Meta struct {
	ID     uint   `json:"id" gorm:"primarykey"`
	Name   string `json:"name"`
	BookID uint   `json:"book_id"`
}

// Blogs App

type Post struct {
	gorm.Model
	Title    string         `json:"title" gorm:"index:idx_post_title,unique"`
	Content  string         `json:"content"`
	Status   sql.NullString `json:"status"`
	Comments []*Comment     `json:"comments" gorm:"foreignKey:PostID"`
	Labels   []*Label       `json:"labels" gorm:"foreignKey:PostID"`
}

type Comment struct {
	gorm.Model
	Content string         `json:"content"`
	Status  sql.NullString `json:"status"`
	PostID  uint           `json:"post_id"`
}

type Label struct {
	Name   string `json:"name"`
	PostID uint   `json:"post_id"`
}
