package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"store-book/entity"
	"store-book/graph/generated"
	"store-book/graph/model"
	"strconv"
)

func (r *commentResolver) ID(ctx context.Context, obj *entity.Comment) (string, error) {
	return strconv.Itoa(int(obj.ID)), nil
}

func (r *commentResolver) Status(ctx context.Context, obj *entity.Comment) (*model.Status, error) {
	return (*model.Status)(&obj.Status.String), nil
}

func (r *commentResolver) CreatedAt(ctx context.Context, obj *entity.Comment, format *bool) (string, error) {
	if *format {
		return obj.CreatedAt.Format("02 January 2006"), nil
	} else {
		return obj.CreatedAt.String(), nil
	}
}

func (r *commentResolver) UpdatedAt(ctx context.Context, obj *entity.Comment) (string, error) {
	return obj.UpdatedAt.Format("02 January 2006"), nil
}

func (r *commentResolver) DeletedAt(ctx context.Context, obj *entity.Comment) (string, error) {
	return obj.DeletedAt.Time.String(), nil
}

func (r *commentResolver) PostID(ctx context.Context, obj *entity.Comment) (string, error) {
	return strconv.Itoa(int(obj.PostID)), nil
}

func (r *labelResolver) PostID(ctx context.Context, obj *entity.Label) (string, error) {
	return strconv.Itoa(int(obj.PostID)), nil
}

func (r *postResolver) ID(ctx context.Context, obj *entity.Post) (string, error) {
	return strconv.Itoa(int(obj.ID)), nil
}

func (r *postResolver) Labels(ctx context.Context, obj *entity.Post, limit *int) ([]*entity.Label, error) {
	labels := r.postService.MyLabels(obj.ID, *limit)
	return labels, nil
}

func (r *postResolver) Comments(ctx context.Context, obj *entity.Post, limit *int) ([]*entity.Comment, error) {
	comments := r.postService.MyComments(obj.ID, *limit)
	return comments, nil
}

func (r *postResolver) Status(ctx context.Context, obj *entity.Post) (*model.Status, error) {
	return (*model.Status)(&obj.Status.String), nil
}

func (r *postResolver) CreatedAt(ctx context.Context, obj *entity.Post, format *bool) (string, error) {
	if *format {
		return obj.CreatedAt.Format("02 January 2006"), nil
	} else {
		return obj.CreatedAt.String(), nil
	}
}

func (r *postResolver) UpdatedAt(ctx context.Context, obj *entity.Post) (string, error) {
	return obj.UpdatedAt.String(), nil
}

func (r *postResolver) DeletedAt(ctx context.Context, obj *entity.Post) (string, error) {
	return obj.DeletedAt.Time.String(), nil
}

// Comment returns generated.CommentResolver implementation.
func (r *Resolver) Comment() generated.CommentResolver { return &commentResolver{r} }

// Label returns generated.LabelResolver implementation.
func (r *Resolver) Label() generated.LabelResolver { return &labelResolver{r} }

// Post returns generated.PostResolver implementation.
func (r *Resolver) Post() generated.PostResolver { return &postResolver{r} }

type commentResolver struct{ *Resolver }
type labelResolver struct{ *Resolver }
type postResolver struct{ *Resolver }
