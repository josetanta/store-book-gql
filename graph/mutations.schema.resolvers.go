package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"store-book/entity"
	"store-book/graph/generated"
	"store-book/graph/model"

	"github.com/99designs/gqlgen/graphql"
	"github.com/vektah/gqlparser/v2/gqlerror"
)

func (r *mutationResolver) AddUser(ctx context.Context, input model.UserInput) (*entity.User, error) {
	newUser, err := r.userService.CreateUser(&input)

	if err != nil {
		return nil, &gqlerror.Error{
			Message: err.Error(),
			Path:    graphql.GetPath(ctx),
		}
	}

	return newUser, nil
}

func (r *mutationResolver) AddBook(ctx context.Context, input model.BookInput) (*entity.Book, error) {
	newBook, err := r.bookService.CreateBook(&input)

	if err != nil {
		return nil, &gqlerror.Error{
			Message: err.Error(),
			Path:    graphql.GetPath(ctx),
		}
	}

	return newBook, nil
}

func (r *mutationResolver) AddMetaToBook(ctx context.Context, input model.MetaInput) (*entity.Meta, error) {
	newMeta, err := r.bookService.CreateMetaToBook(&input)

	if err != nil {
		return nil, &gqlerror.Error{
			Message: err.Error(),
			Path:    graphql.GetPath(ctx),
		}
	}

	return newMeta, nil
}

func (r *mutationResolver) CreatePost(ctx context.Context, input model.PostInput) (*entity.Post, error) {
	newPost, err := r.postService.CreatePost(&input)

	if err != nil {
		return nil, &gqlerror.Error{
			Message: err.Error(),
			Path:    graphql.GetPath(ctx),
		}
	}

	return newPost, nil
}

func (r *mutationResolver) CreateComment(ctx context.Context, input model.CommentInput) (*entity.Comment, error) {
	newComment, err := r.postService.CreateCommentToPost(&input, &r.mutex)
	if err != nil {
		return nil, &gqlerror.Error{
			Message: err.Error(),
			Path:    graphql.GetPath(ctx),
		}
	}
	return newComment, nil
}

func (r *mutationResolver) AddLabelToPost(ctx context.Context, input model.LabelInput) (*entity.Label, error) {
	newLabel, err := r.postService.CreateLabelToPost(&input)

	if err != nil {
		return nil, &gqlerror.Error{
			Message: err.Error(),
			Path:    graphql.GetPath(ctx),
		}
	}
	return newLabel, nil
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

type mutationResolver struct{ *Resolver }
