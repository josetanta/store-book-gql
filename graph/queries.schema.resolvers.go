package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"store-book/entity"
	"store-book/graph/generated"

	"github.com/99designs/gqlgen/graphql"
	"github.com/vektah/gqlparser/v2/gqlerror"
)

func (r *queryResolver) UserByID(ctx context.Context, id string) (*entity.User, error) {
	user, err := r.userService.GetUserById(id)

	if err != nil {
		return nil, &gqlerror.Error{
			Message: err.Error(),
			Path:    graphql.GetPath(ctx),
		}
	}

	return user, nil
}

func (r *queryResolver) UserList(ctx context.Context, limit *int) ([]*entity.User, error) {
	return r.userService.GetUserList(*limit)
}

func (r *queryResolver) BookByID(ctx context.Context, id string) (*entity.Book, error) {
	book, err := r.bookService.GetBookById(id)
	if err != nil {
		return nil, &gqlerror.Error{
			Message: err.Error(),
			Path:    graphql.GetPath(ctx),
		}
	}
	return book, nil
}

func (r *queryResolver) BookList(ctx context.Context, limit *int) ([]*entity.Book, error) {
	return r.bookService.BookList(*limit), nil
}

func (r *queryResolver) MetaList(ctx context.Context, limit *int) ([]*entity.Meta, error) {
	metas := r.bookService.GetMetaList(*limit)
	return metas, nil
}

func (r *queryResolver) PostList(ctx context.Context, limit *int) ([]*entity.Post, error) {
	return r.postService.PostList(*limit), nil
}

func (r *queryResolver) CommentList(ctx context.Context, limit *int) ([]*entity.Comment, error) {
	comments := r.postService.GetCommentList(*limit)
	return comments, nil
}

func (r *queryResolver) LabelList(ctx context.Context, limit *int) ([]*entity.Label, error) {
	labels := r.postService.GetLabelList(*limit)
	return labels, nil
}

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type queryResolver struct{ *Resolver }
