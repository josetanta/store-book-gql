package graph

import (
	"store-book/services"
	"sync"
)

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.

type Resolver struct {
	bookService services.IBookService
	postService services.IPostService
	userService services.IUserService

	mutex sync.Mutex
}

func NewResolver(bookService services.IBookService, postService services.IPostService, userService services.IUserService) *Resolver {
	return &Resolver{
		bookService: bookService,
		postService: postService,
		userService: userService,
	}
}
