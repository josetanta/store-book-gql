package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"store-book/entity"
	"store-book/graph/generated"
	"store-book/graph/model"
	"strconv"
)

func (r *bookResolver) ID(ctx context.Context, obj *entity.Book) (string, error) {
	return strconv.Itoa(int(obj.ID)), nil
}

func (r *bookResolver) Author(ctx context.Context, obj *entity.Book) (*entity.User, error) {
	castId := strconv.Itoa(int(obj.AuthorID))
	author, _ := r.userService.GetUserById(castId)

	return author, nil
}

func (r *bookResolver) Metas(ctx context.Context, obj *entity.Book, limit *int) ([]*entity.Meta, error) {
	metas := r.bookService.MyMetas(obj.ID, *limit)
	return metas, nil
}

func (r *bookResolver) CreatedAt(ctx context.Context, obj *entity.Book, format *bool) (string, error) {
	if *format {
		return obj.CreatedAt.Format("2 Jan 2006 15:04:05"), nil
	} else {
		return obj.CreatedAt.Format("2 Jan 2006 15:04:05"), nil
	}
}

func (r *bookResolver) UpdatedAt(ctx context.Context, obj *entity.Book) (string, error) {
	return obj.UpdatedAt.Format("2 Jan 2006 15:04:05"), nil
}

func (r *bookResolver) DeletedAt(ctx context.Context, obj *entity.Book) (string, error) {
	return obj.DeletedAt.Time.String(), nil
}

func (r *metaResolver) ID(ctx context.Context, obj *entity.Meta) (string, error) {
	return strconv.Itoa(int(obj.ID)), nil
}

func (r *metaResolver) Book(ctx context.Context, obj *entity.Meta) (*entity.Book, error) {
	castId := strconv.Itoa(int(obj.BookID))
	book, _ := r.bookService.GetBookById(castId)

	return book, nil
}

func (r *userResolver) ID(ctx context.Context, obj *entity.User) (string, error) {
	return strconv.Itoa(int(obj.ID)), nil
}

func (r *userResolver) Books(ctx context.Context, obj *entity.User, limit *int) ([]*entity.Book, error) {
	books := r.userService.MyBooks(obj.ID, *limit)
	return books, nil
}

func (r *userResolver) IsStaff(ctx context.Context, obj *entity.User) (bool, error) {
	return obj.IsStaff.Bool, nil
}

func (r *userResolver) Gender(ctx context.Context, obj *entity.User) (model.Gender, error) {
	return model.Gender(obj.Gender.String), nil
}

func (r *userResolver) CreatedAt(ctx context.Context, obj *entity.User, format *bool) (string, error) {
	if *format {
		return obj.CreatedAt.Format("2 Jan 2006 15:04:05"), nil
	} else {
		return obj.CreatedAt.Format("2 Jan 2006 15:04:05"), nil
	}
}

func (r *userResolver) UpdatedAt(ctx context.Context, obj *entity.User) (string, error) {
	return obj.UpdatedAt.Format("2 Jan 2006 15:04:05"), nil
}

func (r *userResolver) DeletedAt(ctx context.Context, obj *entity.User) (string, error) {
	return obj.DeletedAt.Time.String(), nil
}

// Book returns generated.BookResolver implementation.
func (r *Resolver) Book() generated.BookResolver { return &bookResolver{r} }

// Meta returns generated.MetaResolver implementation.
func (r *Resolver) Meta() generated.MetaResolver { return &metaResolver{r} }

// User returns generated.UserResolver implementation.
func (r *Resolver) User() generated.UserResolver { return &userResolver{r} }

type bookResolver struct{ *Resolver }
type metaResolver struct{ *Resolver }
type userResolver struct{ *Resolver }
