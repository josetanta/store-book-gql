package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"store-book/entity"
	"store-book/graph/generated"
)

func (r *subscriptionResolver) SubComments(ctx context.Context) (<-chan []*entity.Comment, error) {
	comments := r.postService.CommentsStream(ctx, &r.mutex)

	return comments, nil
}

// Subscription returns generated.SubscriptionResolver implementation.
func (r *Resolver) Subscription() generated.SubscriptionResolver { return &subscriptionResolver{r} }

type subscriptionResolver struct{ *Resolver }
