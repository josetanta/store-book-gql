package middlewares

import (
	"context"

	"github.com/gin-gonic/gin"
)

type GinContextKey string

var GinContext GinContextKey = "GinContextKey"

func GinContextToContext() gin.HandlerFunc {

	return func(c *gin.Context) {
		ctx := context.WithValue(c.Request.Context(), GinContext, c)
		c.Request = c.Request.WithContext(ctx)
		c.Next()
	}
}
