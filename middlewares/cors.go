package middlewares

import "github.com/gin-gonic/gin"

var (
	methods = "POST"
	headers = "Content-Type, Content-Length, X-CSRF-Token, Authorization"
)

func CORSMiddle() gin.HandlerFunc {

	return func(ctx *gin.Context) {
		ctx.Writer.Header().Set("Sec-WebSocket-Protocol", "graphql-ws")
		ctx.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		ctx.Writer.Header().Set("Access-Control-Allow-Credentials", "false")
		ctx.Writer.Header().Set("Access-Control-Allow-Headers", headers)
		ctx.Writer.Header().Set("Access-Control-Allow-Methods", methods)

		ctx.Next()
	}
}
