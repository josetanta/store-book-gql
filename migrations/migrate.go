package migrations

import (
	m "store-book/entity"

	"gorm.io/gorm"
)

func MigrateModels(db *gorm.DB) {

	db.AutoMigrate(
		&m.User{},
		&m.Book{},
		&m.Meta{},
		&m.Post{},
		&m.Comment{},
		&m.Label{},
	)
}
