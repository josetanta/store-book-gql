package repository

import (
	"store-book/entity"
	"store-book/graph/model"
	"strconv"

	"gorm.io/gorm"
)

type IBookRepository interface {
	Create(*model.BookInput) (*entity.Book, error)
	GetAll(int) []*entity.Book
	CreateMeta(*model.MetaInput) (*entity.Meta, error)
	GetSingle(string) (*entity.Book, error)
	GetAllMetas(string, int) []*entity.Meta
}

type BookRepository struct {
	db *gorm.DB
}

func NewBookRepository(db *gorm.DB) *BookRepository {
	return &BookRepository{
		db: db,
	}
}

func (repo *BookRepository) Create(input *model.BookInput) (*entity.Book, error) {
	idCast, _ := strconv.Atoi(input.AuthorID)

	newBook := &entity.Book{
		Title:       input.Title,
		Description: input.Description,
		AuthorID:    uint(idCast),
	}

	if err := repo.db.Create(newBook).Error; err != nil {
		return nil, gorm.ErrRegistered
	}
	return newBook, nil
}

func (repo *BookRepository) GetAll(limit int) []*entity.Book {
	var books []*entity.Book

	repo.db.Model(&books).
		Limit(limit).
		Order("created_at desc").
		Find(&books)

	return books
}

func (repo *BookRepository) CreateMeta(input *model.MetaInput) (*entity.Meta, error) {

	if _, err := repo.GetSingle(input.BookID); err != nil {
		return nil, err
	}

	idCast, _ := strconv.Atoi(input.BookID)
	newMeta := &entity.Meta{
		BookID: uint(idCast),
		Name:   input.Name,
	}
	repo.db.Create(newMeta).Commit()

	return newMeta, nil
}

func (repo *BookRepository) GetSingle(id string) (*entity.Book, error) {
	var book entity.Book

	if err := repo.db.
		Where("id = ?", id).
		First(&book).
		Error; err != nil {
		return nil, gorm.ErrRecordNotFound
	}

	return &book, nil
}

func (repo *BookRepository) GetAllMetas(id string, limit int) []*entity.Meta {
	var metas []*entity.Meta

	query := repo.db.
		Model(&metas).
		Limit(limit)

	if len(id) == 0 {
		query.
			Find(&metas)
	} else {
		query.
			Where("book_id=?", id).
			Find(&metas)
	}

	return metas
}
