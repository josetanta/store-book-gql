package repository

import (
	"database/sql"
	"store-book/entity"
	"store-book/graph/model"
	"strconv"

	"gorm.io/gorm"
)

type IPostRepository interface {
	GetAll(int) []*entity.Post
	Create(*model.PostInput) (*entity.Post, error)
	CreateLabel(*model.LabelInput) (*entity.Label, error)
	GetSingle(string) (*entity.Post, error)
	CreateComment(*model.CommentInput) (*entity.Comment, error)
	GetAllComments(string, int) []*entity.Comment
	GetAllLabels(string, int) []*entity.Label
}

type PostRepostory struct {
	db *gorm.DB
}

func NewPostRepository(db *gorm.DB) *PostRepostory {

	return &PostRepostory{
		db: db,
	}
}

func (repo *PostRepostory) GetAll(limit int) []*entity.Post {
	var posts []*entity.Post
	repo.db.Limit(limit).Order("created_at desc").Find(&posts)

	return posts
}

func (repo *PostRepostory) Create(input *model.PostInput) (*entity.Post, error) {

	newPost := &entity.Post{
		Title:   input.Title,
		Content: input.Content,
		Status: sql.NullString{
			String: string(input.Status),
			Valid:  input.Status.IsValid(),
		},
	}

	if err := repo.db.Create(newPost).Error; err != nil {
		return nil, gorm.ErrRegistered
	}

	return newPost, nil
}

func (repo *PostRepostory) CreateLabel(input *model.LabelInput) (*entity.Label, error) {

	if _, err := repo.GetSingle(input.PostID); err != nil {
		return nil, gorm.ErrRecordNotFound
	}

	idCast, _ := strconv.Atoi(input.PostID)
	newLabel := &entity.Label{
		Name:   input.Name,
		PostID: uint(idCast),
	}
	repo.db.Create(newLabel).Commit()
	return newLabel, nil
}

func (repo *PostRepostory) GetSingle(id string) (*entity.Post, error) {
	var post entity.Post
	if err := repo.db.
		Model(&post).
		Where("id = ?", id).
		First(&post).
		Error; err != nil {
		return nil, gorm.ErrRecordNotFound
	}

	return &post, nil
}

func (repo *PostRepostory) CreateComment(input *model.CommentInput) (*entity.Comment, error) {

	if _, err := repo.GetSingle(input.PostID); err != nil {
		return nil, gorm.ErrRecordNotFound
	}

	idCast, _ := strconv.Atoi(input.PostID)
	newComment := &entity.Comment{
		Content: input.Content,
		PostID:  uint(idCast),
		Status: sql.NullString{
			String: string(input.Status),
			Valid:  input.Status.IsValid(),
		},
	}

	repo.db.Create(newComment).Commit()

	return newComment, nil
}

func (repo *PostRepostory) GetAllComments(id string, limit int) []*entity.Comment {
	var comments []*entity.Comment
	query := repo.db.
		Model(&comments).
		Limit(limit).
		Order("created_at desc")

	if len(id) == 0 {
		query.
			Find(&comments)
	} else {
		query.
			Where("post_id=?", id).
			Find(&comments)
	}
	return comments
}

func (repo *PostRepostory) GetAllLabels(id string, limit int) []*entity.Label {
	var labels []*entity.Label
	query := repo.db.
		Model(&labels).
		Limit(limit).
		Order("name asc")

	if len(id) == 0 {
		query.
			Find(&labels)
	} else {
		query.
			Where("post_id=?", id).
			Find(&labels)
	}
	return labels
}
