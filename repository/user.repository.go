package repository

import (
	"database/sql"
	"store-book/entity"
	"store-book/graph/model"

	"gorm.io/gorm"
)

type IUserRepository interface {
	Create(*model.UserInput) (*entity.User, error)
	GetSingle(string) (*entity.User, error)
	GetAll(int) []*entity.User
	Books(string, int) []*entity.Book
}

type UserRepository struct {
	db *gorm.DB
}

func NewUserRepository(db *gorm.DB) *UserRepository {
	return &UserRepository{
		db: db,
	}
}

func (repo *UserRepository) Create(input *model.UserInput) (*entity.User, error) {
	newUser := &entity.User{
		Email: input.Email,
		Name:  input.Name,
		Books: []*entity.Book{},
		Gender: sql.NullString{
			String: string(*input.Gender),
			Valid:  input.Gender.IsValid(),
		},
	}

	if err := repo.db.Create(newUser).Error; err != nil {
		return nil, gorm.ErrRegistered
	}

	return newUser, nil
}

func (repo *UserRepository) GetSingle(id string) (*entity.User, error) {
	var user entity.User

	if err := repo.db.First(&user, id).Error; err != nil {
		return nil, gorm.ErrRecordNotFound
	}

	return &user, nil
}

func (repo *UserRepository) GetAll(limit int) []*entity.User {
	var users []*entity.User
	repo.db.Model(&users).
		Limit(limit).
		Order("created_at desc").
		Find(&users)

	return users
}

func (repo *UserRepository) Books(id string, limit int) []*entity.Book {
	var books []*entity.Book

	repo.db.Model(books).
		Where("author_id=?", id).
		Limit(limit).
		Find(&books)

	return books
}
