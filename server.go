package main

import (
	"log"
	"os"
	"store-book/database"
	"store-book/graph"
	"store-book/graph/generated"
	"store-book/middlewares"
	"store-book/migrations"
	"store-book/repository"
	"store-book/services"

	"github.com/99designs/gqlgen/graphql"
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/gin-gonic/gin"
)

const defaultPort = "8080"

func init() {
	migrations.MigrateModels(database.DB)
}

func graphqlHandler() gin.HandlerFunc {
	h := handler.NewDefaultServer(getSchemaAndResolver())
	return func(ctx *gin.Context) {
		h.ServeHTTP(ctx.Writer, ctx.Request)
	}
}

func playgroundHandler() gin.HandlerFunc {
	h := playground.Handler("Store Book GQL", "/query")

	return func(ctx *gin.Context) {
		h.ServeHTTP(ctx.Writer, ctx.Request)
	}
}

func getSchemaAndResolver() graphql.ExecutableSchema {
	bookRep := repository.NewBookRepository(database.DB)
	postRep := repository.NewPostRepository(database.DB)
	userRep := repository.NewUserRepository(database.DB)

	bookServ := services.NewBookService(bookRep)
	postServ := services.NewPostService(postRep)
	userServ := services.NewUserService(userRep)

	res := graph.NewResolver(bookServ, postServ, userServ)
	es := generated.NewExecutableSchema(generated.Config{Resolvers: res})

	return es
}

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = defaultPort
	}

	r := gin.Default()
	// r.SetTrustedProxies([]string{"127.0.0.1", "localhost"})
	r.Use(middlewares.CORSMiddle())
	r.Use(middlewares.GinContextToContext())
	r.Any("/query", graphqlHandler())
	r.GET("/", playgroundHandler())

	log.Fatal(r.Run(":" + port))

}
