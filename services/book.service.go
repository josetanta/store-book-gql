package services

import (
	"store-book/entity"
	"store-book/graph/model"
	"store-book/repository"
	"strconv"
)

type IBookService interface {
	BookList(int) []*entity.Book
	CreateBook(*model.BookInput) (*entity.Book, error)
	CreateMetaToBook(input *model.MetaInput) (*entity.Meta, error)
	GetBookById(string) (*entity.Book, error)
	MyMetas(uint, int) []*entity.Meta
	GetMetaList(int) []*entity.Meta
}

type BookService struct {
	repo repository.IBookRepository
}

func NewBookService(repo repository.IBookRepository) *BookService {
	return &BookService{
		repo: repo,
	}
}

func (sv *BookService) BookList(limit int) []*entity.Book {
	return sv.repo.GetAll(limit)
}

func (sv *BookService) CreateBook(input *model.BookInput) (*entity.Book, error) {
	return sv.repo.Create(input)
}

func (sv *BookService) CreateMetaToBook(input *model.MetaInput) (*entity.Meta, error) {
	return sv.repo.CreateMeta(input)
}

func (sv *BookService) GetBookById(id string) (*entity.Book, error) {

	return sv.repo.GetSingle(id)
}

func (sv *BookService) MyMetas(id uint, limit int) []*entity.Meta {
	castId := strconv.Itoa(int(id))
	return sv.repo.GetAllMetas(castId, limit)
}

func (sv *BookService) GetMetaList(limit int) []*entity.Meta {

	return sv.repo.GetAllMetas("", limit)
}
