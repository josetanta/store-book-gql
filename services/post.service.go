package services

import (
	"context"
	"store-book/entity"
	"store-book/graph/model"
	"store-book/repository"
	"strconv"
	"sync"
)

type IPostService interface {
	PostList(int) []*entity.Post
	CreatePost(*model.PostInput) (*entity.Post, error)
	CreateLabelToPost(*model.LabelInput) (*entity.Label, error)
	GetPostById(string) (*entity.Post, error)
	CreateCommentToPost(*model.CommentInput, *sync.Mutex) (*entity.Comment, error)
	MyComments(uint, int) []*entity.Comment
	GetCommentList(int) []*entity.Comment
	MyLabels(uint, int) []*entity.Label
	GetLabelList(int) []*entity.Label
	SubscriptionComments(string, *sync.Mutex)
	CommentsStream(context.Context, *sync.Mutex) chan []*entity.Comment
}

type PostService struct {
	repo            repository.IPostRepository
	commentsObserve chan []*entity.Comment
}

func NewPostService(repo repository.IPostRepository) *PostService {
	newService := &PostService{
		repo:            repo,
		commentsObserve: make(chan []*entity.Comment, 1),
	}
	return newService
}

func (sv *PostService) PostList(limit int) []*entity.Post {
	return sv.repo.GetAll(limit)
}

func (sv *PostService) CreatePost(input *model.PostInput) (*entity.Post, error) {

	return sv.repo.Create(input)
}

func (sv *PostService) CreateLabelToPost(input *model.LabelInput) (*entity.Label, error) {
	return sv.repo.CreateLabel(input)
}

func (sv *PostService) GetPostById(id string) (*entity.Post, error) {
	return sv.repo.GetSingle(id)
}

// Create and Subscription (Websockets).
// Require params: CommentInput, *sync.Mutex
func (sv *PostService) CreateCommentToPost(input *model.CommentInput, mu *sync.Mutex) (*entity.Comment, error) {
	c, err := sv.repo.CreateComment(input)

	if err != nil {
		return nil, err
	} else {
		sv.SubscriptionComments(input.PostID, mu)
		return c, nil
	}
}

func (sv *PostService) MyComments(id uint, limit int) []*entity.Comment {
	idCast := strconv.Itoa(int(id))
	return sv.repo.GetAllComments(idCast, limit)
}

func (sv *PostService) GetCommentList(limit int) []*entity.Comment {
	return sv.repo.GetAllComments("", limit)
}

func (sv *PostService) MyLabels(id uint, limit int) []*entity.Label {
	idCast := strconv.Itoa(int(id))
	return sv.repo.GetAllLabels(idCast, limit)
}

func (sv *PostService) GetLabelList(limit int) []*entity.Label {
	return sv.repo.GetAllLabels("", limit)
}

func (sv *PostService) SubscriptionComments(idPost string, mu *sync.Mutex) {
	idCast, _ := strconv.Atoi(idPost)
	allComments := sv.MyComments(uint(idCast), 100)

	mu.Lock()
	sv.commentsObserve <- allComments
	mu.Unlock()
}

func (sv *PostService) CommentsStream(ctx context.Context, mu *sync.Mutex) chan []*entity.Comment {
	comments := make(chan []*entity.Comment, 1)
	go func() {
		<-ctx.Done()
	}()

	mu.Lock()
	sv.commentsObserve = comments
	mu.Unlock()

	return comments
}
