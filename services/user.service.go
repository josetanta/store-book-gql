package services

import (
	"store-book/entity"
	"store-book/graph/model"
	"store-book/repository"
	"strconv"
)

type IUserService interface {
	CreateUser(*model.UserInput) (*entity.User, error)
	GetUserById(string) (*entity.User, error)
	GetUserList(int) ([]*entity.User, error)
	MyBooks(uint, int) []*entity.Book
}

type UserService struct {
	repo repository.IUserRepository
}

func NewUserService(repo repository.IUserRepository) *UserService {
	return &UserService{
		repo: repo,
	}
}

func (sv *UserService) CreateUser(input *model.UserInput) (*entity.User, error) {
	return sv.repo.Create(input)
}

func (sv *UserService) GetUserById(id string) (*entity.User, error) {
	return sv.repo.GetSingle(id)
}

func (sv *UserService) GetUserList(limit int) ([]*entity.User, error) {
	return sv.repo.GetAll(limit), nil
}

func (sv *UserService) MyBooks(id uint, limit int) []*entity.Book {
	castId := strconv.Itoa(int(id))
	return sv.repo.Books(castId, limit)
}
